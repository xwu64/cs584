from PIL import Image
import numpy as np
import csv
from os import listdir

def process_img(filename):
    image = Image.open(filename)
    box = (80,0,560,480)
    _img = image.crop(box)
    _img = _img.resize((64, 64))
    _res = np.asarray(_img.getdata())
    return np.asarray([p/255.0 for p in _res])


if __name__=="__main__":
    filename = 'ExtendedYaleB/yaleB11/yaleB11_P00A+000E+00.pgm'
    root = 'ExtendedYaleB'
    xfh = open("X.csv",'w')
    xwriter = csv.writer(xfh)
    yfh = open("y.csv",'w')
    ywriter = csv.writer(yfh)
    y_name = 'yaleB11'

    for folder in listdir(root):
        if not 'yale' in folder: continue
        for name in listdir(root+'/'+folder):
            filename = root+'/'+folder+'/'+name
            if not '.pgm' in name:
                print (filename)
                continue
            _img = process_img(filename)
            xwriter.writerow(_img)

            if folder==y_name: 
                ywriter.writerow([1])
                print('1:',filename)
            else: ywriter.writerow([0])

    xfh.close()
    yfh.close()

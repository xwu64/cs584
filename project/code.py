import numpy as np
import matplotlib.pyplot as plt

#TODO: generate matrix one by one to avoid out of memory

def to_matrix(filename):
    fh = open(filename, 'r')
    res = []
    for ln, line in enumerate(fh):
        img = np.zeros((64,64))
        for i, value in enumerate(line.split(' ')):
            img[int(i/64)][i%64]=float(value)
        res.append(img)
    return res

def to_imgs(datas, path):
    for i,img in enumerate(datas):
        plt.imsave('%d.png'%i,img)

if __name__=='__main__':
    datas = to_matrix('data/X.csv')
    to_imgs(datas,'imgs/')

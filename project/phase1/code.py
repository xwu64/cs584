import pickle
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import StratifiedKFold, cross_validate
from sklearn.svm import SVC

#TODO: generate matrix one by one to avoid out of memory

def get_X():
    fh = open('../data/X.csv', 'r')
    res = []
    for ln, line in enumerate(fh):
        img = np.zeros((4096))
        for i, value in enumerate(line.split(' ')):
            img[int(i)]=float(value)
        res.append(img)
    return np.array(res)

def get_y(filename):
    fh = open('../data/%s'%filename, 'r')
    res = []
    for line in fh:
        res.append(int(line))
    return np.array(res)

def to_imgs(datas, path):
    for i,img in enumerate(datas):
        plt.imsave('%d.png'%i,img)

avg = lambda x: sum(x)/len(x)

def knn(X,y,name,n_jobs):
    res = []
    res2 = []
    for n in range(1,6,2):
        clf = KNeighborsClassifier(n_neighbors=n,n_jobs=n_jobs)
        stratified_cv_results = cross_validate(clf, X, y,
                cv=StratifiedKFold(n_splits = 3, shuffle=True, random_state = 4020),
                scoring=('precision', 'recall','f1'), return_train_score=False, n_jobs=n_jobs)
        res.append(type(stratified_cv_results))
        res.append(stratified_cv_results)
        res2.append(avg(stratified_cv_results['test_f1']))
    fh = open('%s_knn.log'%name,'w')
    fh.write(str(res))
    fh.close()
    print(res2)
    return res2

def svc_test(X,y,name):
    test_f1=-999999999
    kernels = ['linear', 'poly', 'rbf', 'sigmoid']
    for c in range(-4,-3):
        for k in kernels[:1]:
            if k=='linear':
                print('[%d] [%s] test'%(c, k))
                clf = SVC(C=2**c, kernel=k)
                stratified_cv_results = cross_validate(clf, X, y,
                        cv=StratifiedKFold(n_splits = 3, shuffle=True, random_state = 4020),
                        scoring=('precision', 'recall', 'f1'), return_train_score=False, n_jobs=n_jobs)
                avg_fit = avg(stratified_cv_results['test_f1'])
                if avg_fit>test_f1:
                    test_f1 = avg_fit
                    fh = open('%s.log'%name,'w')
                    res = [c, k, stratified_cv_results]
                    fh.write(str(res))
                    fh.close()
            elif k=='poly':
                for d in range(1,8):
                    print('[%d] [%s] [%s] test'%(c, k, d))
                    clf = SVC(C=2**c, kernel=k, degree=d)
                    stratified_cv_results = cross_validate(clf, X, y,
                            cv=StratifiedKFold(n_splits = 3, shuffle=True, random_state = 4020),
                            scoring=('precision', 'recall', 'f1'), return_train_score=False, n_jobs=n_jobs)
                    avg_fit = avg(stratified_cv_results['test_f1'])
                    if avg_fit>test_f1:
                        test_f1 = avg_fit
                        fh = open('%s.log'%name,'w')
                        res = [c, k,d, stratified_cv_results]
                        fh.write(str(res))
                        fh.close()
            elif k in ['rbf', 'sigmoid']:
                for ga in range(-8,2):
                    print('[%d] [%s] [%s] test'%(c, k, ga))
                    clf = SVC(C=2**c, kernel=k, gamma=2**ga)
                    stratified_cv_results = cross_validate(clf, X, y,
                            cv=StratifiedKFold(n_splits = 3, shuffle=True, random_state = 4020),
                            scoring=('precision', 'recall', 'f1'), return_train_score=False, n_jobs=n_jobs)
                    avg_fit = avg(stratified_cv_results['test_f1'])
                    if avg_fit>test_f1:
                        test_f1 = avg_fit
                        fh = open('%s.log'%name,'w')
                        res = [c, k,ga, stratified_cv_results]
                        fh.write(str(res))
                        fh.close()
    return test_f1




def test(X, name, n_jobs):
    filename = '%s.csv'%name
    print('[%s] getting y' % name)
    y = get_y(filename)
    print('svc')
    res2 = svc_test(X,y,name)
    print('knn')
    res1 = knn(X,y,name,n_jobs)

    pickle.dump((res1, res2), open('%s.pkl'%name, 'wb'))

if __name__=='__main__':
    print('getting x')
    X = get_X()
    n_jobs=-1
    test(X,'bush', n_jobs)
    test(X,'williams', n_jobs)


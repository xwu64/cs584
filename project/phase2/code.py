import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import StratifiedKFold, cross_validate
from sklearn.svm import SVC
import pickle
from sklearn.decomposition import PCA


def get_X():
    fh = open('X.csv', 'r')
    res = []
    for ln, line in enumerate(fh):
        img = np.zeros((4096))
        for i, value in enumerate(line.split(' ')):
            img[int(i)]=float(value)
        res.append(img)
    return np.array(res)


def get_y(filename):
    fh = open('%s'%filename, 'r')
    res = []
    for line in fh:
        res.append(int(line))
    return np.array(res)

def to_imgs(datas, path):
    for i,img in enumerate(datas):
        plt.imsave('%d.png'%i,img)

avg = lambda x: sum(x)/len(x)

def knn(X,y,name,n_jobs):
    res = []
    res2 = []
    n = 1
    clf = KNeighborsClassifier(n_neighbors=n,n_jobs=n_jobs)
    stratified_cv_results = cross_validate(clf, X, y,
            cv=StratifiedKFold(n_splits = 3, shuffle=True, random_state = 4020),
            scoring=('precision', 'recall','f1'), return_train_score=False, n_jobs=n_jobs)
    res.append(type(stratified_cv_results))
    res.append(stratified_cv_results)
    res2.append(avg(stratified_cv_results['test_f1']))
    fh = open('%s_knn.log'%name,'w')
    fh.write(str(res))
    fh.close()
    print(res2)
    return res2

def svc_test(X,y,name):
    test_f1=-999999999
    k = 'linear'
    for c in range(-4,-3):
        print('[%d] [%s] test'%(c, k))
        clf = SVC(C=2**c, kernel=k)
        stratified_cv_results = cross_validate(clf, X, y,
                cv=StratifiedKFold(n_splits = 3, shuffle=True, random_state = 4020),
                scoring=('precision', 'recall', 'f1'), return_train_score=False, n_jobs=n_jobs)
        avg_fit = avg(stratified_cv_results['test_f1'])
        if avg_fit>test_f1:
            test_f1 = avg_fit
            fh = open('%s.log'%name,'w')
            res = [c, k, stratified_cv_results]
            fh.write(str(res))
            fh.close()

    return test_f1



    #pickle.dump((res1, res2), open('%s.pkl'%name, 'wb'))


def truncate(X, n, solver):
  pca = PCA(n_components = n, svd_solver = solver )
  trunc = pca.fit_transform(X)
  return trunc


if __name__=='__main__':
    X = get_X()
    print ("x loading successful")

    for name in ['bush','williams']:
        filename = '%s.csv' % name
        print('[%s] getting y' % name)
        y = get_y(filename)

        res2_svc = -999999999
        res1_knn = -999999999
        best_solver_svc = '1'
        best_n_svc = 0
        best_solver_knn = '1'
        best_n_knn = 0

        for solver in ['auto', 'full', 'randomized']:
            for n in range(512, 4096, 512):

                X = truncate(X, n, solver)
                n_jobs = -1

                res2 = svc_test(X, y, name)
                if res2 > res2_svc:
                    res2_svc = res2
                    best_solver_svc = solver
                    best_n_svc = n

                res1 = knn(X, y, name, n_jobs)
                if res1 > res1_knn:
                    res1_knn = res1
                    best_solver_knn = solver
                    best_n_knn = n

                print("auto", "512")


        print ("knn:", best_solver_knn, best_n_knn)
        print ("svc:", best_solver_svc, best_n_svc)
        pickle.dump((res1_knn, res2_svc), open('%s.pkl' % name, 'wb'))


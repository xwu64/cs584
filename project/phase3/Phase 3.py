
import numpy as np
import pickle
from sklearn.model_selection import train_test_split
import keras as K
from keras.models import Sequential,Input,Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score

#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

def get_X():
    fh = open('X.csv', 'r')
    res = []
    for ln, line in enumerate(fh):
        if ln>100:
            break
        else:
            img = np.zeros(64, 64)
            for i, value in enumerate(line.split(' ')):

                img[int(i/64)][int(i%64)]=float(value)
        res.append(img)
    return np.array(res)


def get_y(filename):
    fh = open('%s'%filename, 'r')
    res = []
    for ln, line in enumerate(fh):
        if ln>100:
            break
        res.append(int(line))
    return np.array(res)


def split_sample(X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify = y, test_size=0.33, random_state= 4020, shuffle=True)
    return X_train, X_test, y_train,y_test

def f1(y_true, y_pred):
    def recall(y_true, y_pred):

        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):

        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))


def cnn(n, X_train, X_test, y_train, y_test):
    batch_size = 64
    epochs = 20
    num_classes = 2
    drop_out = 0.25
    relu_alpha = 0.1

    neural_model = Sequential()
    for i in range(1, n + 1):
        neural_model.add(Conv2D(64 * n, kernel_size=(3, 3), activation='linear', padding='same', input_shape=(64, 64, 1)))
        neural_model.add(LeakyReLU(alpha=relu_alpha))
        neural_model.add(MaxPooling2D((2, 2), padding='same'))
        neural_model.add(Dropout(drop_out))

    neural_model.add(Flatten())
    neural_model.add(Dense(n*64, activation='linear'))
    neural_model.add(LeakyReLU(alpha=relu_alpha))
    neural_model.add(Dropout(drop_out))
    neural_model.add(Dense(num_classes, activation='sigmoid'))

    neural_model.compile(loss=k.losses.categorical_crossentropy, optimizer=k.optimizers.Adam(),metrics=['f1'])
    neural_train = neural_model.fit(X_train, y_train, batch_size=batch_size,
                                    epochs=epochs, verbose=1)
    neural_test_eval = neural_model.evaluate(X_train, y_train, verbose=0)
    neural_test_eval = neural_model.evaluate(X_test, y_test, verbose=0)

    return neural_test_eval, neural_test_eval

if __name__=='__main__':
    X = get_X()
    print("x loading successful", len(X))

    for name in ['bush']:
        filename = '%s.csv' % name
        print('[%s] getting y' % name)
        y = get_y(filename)
        print("y length", len(y))

        X_train, X_test, y_train, y_test = split_sample(X, y)
        train_f1, test_f1 = cnn(2, X_train, X_test, y_train, y_test)

    print (train_f1, test_f1)


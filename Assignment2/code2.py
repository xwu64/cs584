import pickle
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import BernoulliNB
from math import e, log
import numpy as np
from time import sleep

def metric(clf, X, y):
    res = 0
    for i,x in enumerate(X):
        res+=clf.class_log_prior_[int(y[i])]
        for j, x_j in enumerate(x):
            if x_j==True: res+=clf.feature_log_prob_[int(y[i])][j]
            else: res+=log(1-(e**clf.feature_log_prob_[int(y[i])][j]))
    return res

Xs = pickle.load(open('binarized_xs.pkl', 'rb'))
ys = pickle.load(open('binarized_ys.pkl', 'rb'))

train_jll = np.zeros((10, 15))
test_jll = np.zeros((10, 15))

for i in range(10):
    xi, yi = Xs[i], ys[i]
    X_train, X_test, y_train, y_test = train_test_split(xi, yi, test_size=1./3, random_state=4020)
    for j in range(-7,8):
        alpha = 10**j
        clf = BernoulliNB(alpha=alpha)
        clf.fit(X_train, y_train)
        jll = clf._joint_log_likelihood(X_train)
        print (jll)
        sleep(100)
        for k,y_i in enumerate(y_train):
            train_jll[i][j]+=jll[k][int(y_i)]
        jll = clf._joint_log_likelihood(X_test)
        for k,y_i in enumerate(y_test):
            test_jll[i][j]+=jll[k][int(y_i)]


print("Train set joint log likelihood")
for i in range(10):
	print("\t".join("{0:.4f}".format(n) for n in train_jll[i]))


print("\nTest set joint log likelihood")
for i in range(10):
	print("\t".join("{0:.4f}".format(n) for n in test_jll[i]))


pickle.dump((train_jll, test_jll), open('result2.pkl', 'wb'))

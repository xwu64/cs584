import pickle
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import numpy as np
from math import log,e
import matplotlib.pyplot as plt

def get_complexity(panelty,clf):
    if panelty=='l2':
        return sum( ((clf.coef_**2)[0]) )  + clf.intercept_[0]**2
    elif panelty=='l1':
        return sum(abs(clf.coef_[0])) + abs(clf.intercept_[0])
    else: print('invalid panelty')

def get_cll(clf, X, y):
    proba = clf.predict_log_proba(X)
    res = 0
    for i, p in enumerate(proba):
        res+=p[int(y[i])]
    return res

def get_nzw(clf):
    res = 0
    if clf.intercept_[0]==0:  res+=1
    for w in clf.coef_[0]:
        if w==0: res+=1
    return res

def plot(x1, x2, y1,y2,mod, seq):
    '''
    mod==0:  train_cll and test_cll
    mod==1:  l2_num_zero and l1_num_zero
    '''
    fig, ax = plt.subplots()
    ax.set_title('D%d'%seq)
    if mod==0:
        ax.plot(x1, y1, label='train_cll')
        ax.plot(x2, y2, label='test_cll')
        ax.legend()
        plt.savefig('cll_%d.pdf'%seq)
    elif mod==1:
        ax.plot(x1, y1, label='l2_num_zero')
        ax.plot(x2, y2, label='l1_num_zero')
        ax.legend()
        plt.savefig('numZ_%d.pdf'%seq)

Xs = pickle.load(open('binarized_xs.pkl', 'rb'))
ys = pickle.load(open('binarized_ys.pkl', 'rb'))

l2_model_complexity = np.zeros((10,15))
l2_train_cll = np.zeros((10,15))
l2_test_cll = np.zeros((10,15))
l2_num_zero_weights = np.zeros((10,15))
l1_num_zero_weights = np.zeros((10,15))

for i in range(10):
    xi, yi = Xs[i], ys[i]
    X_train, X_test, y_train, y_test = train_test_split(xi, yi, test_size=1./3, random_state=4020)
    for j in range(-7,8):
        c = 10**j

        #l2
        clf = LogisticRegression(penalty='l2',C=c, random_state=42)
        clf.fit(X_train, y_train)

        l2_model_complexity[i][j+7]=get_complexity('l2',clf)
        l2_train_cll[i][j+7]=get_cll(clf,X_train, y_train)
        l2_test_cll[i][j+7]=get_cll(clf,X_test, y_test)
        l2_num_zero_weights[i][j+7]=get_nzw(clf)

        #l1
        clf = LogisticRegression(penalty='l1',C=c, random_state=42)
        clf.fit(X_train, y_train)
        l1_num_zero_weights[i][j+7]=get_nzw(clf)

    plot(l2_model_complexity[i], l2_model_complexity[i], l2_train_cll[i], l2_test_cll[i], 0, i)
    plot(range(-7,8), range(-7,8), l2_num_zero_weights[i], l1_num_zero_weights[i], 1, i)

print("l2_model_complexity")
for i in range(10):
	print("\t".join("{0:.4f}".format(n) for n in l2_model_complexity[i]))

print("l2_train_cll")
for i in range(10):
	print("\t".join("{0:.4f}".format(n) for n in l2_train_cll[i]))

print("l2_test_cll")
for i in range(10):
	print("\t".join("{0:.4f}".format(n) for n in l2_test_cll[i]))

print("l2_num_zero_weights")
for i in range(10):
	print("\t".join("{0:.4f}".format(n) for n in l2_num_zero_weights[i]))

print("l1_num_zero_weights")
for i in range(10):
	print("\t".join("{0:.4f}".format(n) for n in l1_num_zero_weights[i]))

pickle.dump((l2_model_complexity, l2_train_cll, l2_test_cll, l2_num_zero_weights, l1_num_zero_weights), open('result.pkl', 'wb'))
